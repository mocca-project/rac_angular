import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './Services/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/signin', pathMatch: 'full' },
      {
        path: 'signin',
        loadChildren: './Login/signin/signin.module#SigninModule'
      },
      {
        path: 'main',
        loadChildren: './Main/mainpage/mainpage.module#MainpageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
