import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../../material-module';
import { UsershowComponent, EditUserDialogComponent } from './usershow/usershow.component';
import { UsercreateComponent } from './usercreate/usercreate.component';

@NgModule({
  entryComponents: [EditUserDialogComponent],
  declarations: [
    UserComponent,
    UsershowComponent,
    EditUserDialogComponent,
    UsercreateComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    DemoMaterialModule,
    RouterModule
  ]
})
export class UserModule { }
