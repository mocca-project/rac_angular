import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../Shares/user.service';
import { UsersService } from '../../../../Services/users.service';
import { SaveModel } from '../../../../Model/save.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usercreate',
  templateUrl: './usercreate.component.html',
  styleUrls: ['./usercreate.component.css']
})
export class UsercreateComponent implements OnInit {
  username?: string = '';
  password?: string = '';
  confirmpassword?: string = '';
  name?: string = '';
  email?: string = '';
  phone?: string = '';
  address?: string = '';
  gender?: string = '';
  position?: string = '';
  image?: string = '';
  // tslint:disable-next-line:variable-name
  role_id?: number = 0;

  role: string = '';
  isPassHide = true;
  isConPassHide = true;

  // image
  imageValue: any;
  selectedFiles: File;

  // save
  save: SaveModel;

  constructor(
    private alertUser: UserService,
    private userService: UsersService,
    private routes: Router,
  ) { }

  ngOnInit() {
    this.image = '/assets/user/user.png';
  }

  ImageToBase64(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.image = evented.target.result;
        this.imageValue = this.image.split(',')[1];
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      // this.imageService.alertImageOverSize();
    }
  }

  createUser() {
    if (this.username !== '' && this.password !== '' && this.name !== '' && this.email !== '' && this.phone !== '' && this.gender !== '' && this.position !== '' && this.role !== '' && this.password !== '' && this.confirmpassword !== '') {
      if (this.password === this.confirmpassword) {

        if (this.role === 'User') {
          this.role_id = 1;
        } else if (this.role === 'Volunteer') {
          this.role_id = 2;
        } else if (this.role === 'Admin') {
          this.role_id = 3;
        }

        const userinfo = {
          username: this.username,
          password: this.password,
          name: this.name,
          email: this.email,
          phone: this.phone,
          address: this.address,
          gender: this.gender,
          position: this.position,
          image: this.imageValue,
          role_id: this.role_id
        };

        this.alertUser.alertConfirmCreateUser().then(resolve => {
          if (resolve) {
            this.userService.createUser(userinfo).subscribe(data => {
              this.save = data;
              if (this.save.error) {
                this.alertUser.alertErrorCreateUser();
              } else {
                this.alertUser.alertSuccessCreateUser();
                this.routes.navigate(['../main/users']);
              }
            });
          }
        });
      } else {
        this.alertUser.alertNotSamePassword();
      }
    } else {
      this.alertUser.alertSomeTextNull();
    }
  }

}
