import { Component, OnInit, Inject } from '@angular/core';
import { User, UserInfo } from '../../../../Model/user.model';
import { Host } from '../../../../../environments/environment';
import { UsersService } from '../../../../Services/users.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ImageService } from '../../../../Shares/image.service';
import { UserService } from '../../../../Shares/user.service';
import { SaveModel } from '../../../../Model/save.model';

@Component({
  selector: 'app-usershow',
  templateUrl: './usershow.component.html',
  styleUrls: ['./usershow.component.css']
})
export class UsershowComponent implements OnInit {

  HOST_URL = Host.url;

  user;

  userDetail: UserInfo;

  order = 'id ASC';
  offset = 0;
  limit = 10;
  keyword = '';

  isLoadUserList = false;
  isLoadDetail = false;

  idDetail;
  nameDetail;
  emailDetail;
  phoneDetail;
  addressDetail;
  genderDetail;
  positionDetail;
  imageDetail;
  roleDetail;
  verifyDetail = false;
  verifyImageDetail;

  // image profile,verify modal
  showImage;
  showImageVerify;

  // save
  save: SaveModel;

  constructor(
    private userService: UsersService,
    public dialog: MatDialog,
    private alertUser: UserService,
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.user = '';
    this.isLoadUserList = true;
    const params = {
      // order: this.order,
      // offset: this.offset,
      // limit: this.limit,
      keyword: this.keyword
    };

    this.userService.getuserService(params).subscribe(data => {
      // console.log(data);
      this.user = data;
      this.isLoadUserList = false;
    });
  }

  checkTabSelect(event) {
    if (event.index === 0) { // all user
      this.keyword = '';
      this.getUser();
    } else if (event.index === 1) { // users
      this.keyword = '1';
      this.getUser();
    } else if (event.index === 2) { // volunteer
      this.keyword = '2';
      this.getUser();
    } else if (event.index === 3) { // admin
      this.keyword = '3';
      this.getUser();
    }
  }


  getUserDetail(id) {
    this.userDetail = null;
    this.isLoadDetail = true;

    this.userService.getuserDetail(id).subscribe(data => {
      this.userDetail = data;

      this.idDetail = this.userDetail.id;
      this.nameDetail = this.userDetail.name;
      this.emailDetail = this.userDetail.email;
      this.phoneDetail = this.userDetail.phone;
      this.addressDetail = this.userDetail.address;
      this.genderDetail = this.userDetail.gender;
      this.positionDetail = this.userDetail.position;
      this.imageDetail = this.HOST_URL + this.userDetail.image;
      this.verifyDetail = this.userDetail.verify;
      this.verifyImageDetail = this.HOST_URL + this.userDetail.verify_image;

      if (this.userDetail) {
        if (this.userDetail.role_id === 1) {
          this.roleDetail = 'User';
        } else if (this.userDetail.role_id === 2) {
          this.roleDetail = 'Volunteer';
        } else if (this.userDetail.role_id === 3) {
          this.roleDetail = 'Admin';
        }
        this.isLoadDetail = false;
      } else {
        this.isLoadDetail = false;
      }
    });
  }

  editProfile() {
    this.openDialog();
  }

  openDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      width: '500px',
      data: {
        id: this.idDetail,
        name: this.nameDetail,
        email: this.emailDetail,
        phone: this.phoneDetail,
        address: this.addressDetail,
        gender: this.genderDetail,
        position: this.positionDetail,
        image: this.imageDetail,
        role: this.roleDetail,
        verify: this.verifyDetail,
        image_verify: this.verifyImageDetail
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getUser();
      this.getUserDetail(this.idDetail);
    });
  }

  deleteUser(id) {
    this.alertUser.alertConfirmDeleteUser().then(resolve => {
      if (resolve) {
        this.userService.deleteUser(id).subscribe(data => {
          this.save = data;
          if (this.save.error) {

          } else {
            this.alertUser.alertSuccessDeleteUser();
            this.userDetail = null;
            this.getUser();
          }
        });
      }
    });
  }

  selectImageProfile(image) {
    this.showImage = image;
  }

  selectImageVerify(image) {
    this.showImageVerify = image;
  }
}

export interface DialogData {
  id: any;
  name: any;
  email: any;
  phone: any;
  address: any;
  gender: any;
  position: any;
  image: any;
  role: any;
  verify: any;
  verify_image: any;
}

@Component({
  selector: 'app-detailedit-dialog',
  templateUrl: 'userdetailedit-dialog.html',
})
export class EditUserDialogComponent implements OnInit {

  HOST_URL = Host.url;

  user: User;

  // image
  imageValue: any;
  verifyimageValue: any = '';
  selectedFiles: File;

  // save
  save: SaveModel;

  constructor(
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private imageService: ImageService,
    private userService: UsersService,
    private alertUser: UserService,
  ) { }

  ngOnInit() {
    if (this.data.verify === null) {
      this.data.verify = false;
    }
    this.getuser();
  }

  getuser() {
    this.userService.getUser().subscribe(data => {
      this.user = data;
    });
  }

  ImageToBase64(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.data.image = evented.target.result;
        this.imageValue = this.data.image.split(',')[1];
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      this.imageService.alertImageOverSize();
    }
  }

  ImageToBase64Verify(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.data.verify_image = evented.target.result;
        this.verifyimageValue = this.data.verify_image.split(',')[1];
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      this.imageService.alertImageOverSize();
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveProfile() {
    if (this.data.name !== '' && this.data.email !== '' && this.data.phone !== '' && this.data.gender !== '' && this.data.position !== '' && this.data.role !== '') {
      if ((this.data.verify === false && this.verifyimageValue === '') || (this.data.verify !== false && this.verifyimageValue !== '')) {

        let roleid;
        if (this.data.role === 'User') {
          roleid = 1;
        } else if (this.data.role === 'Volunteer') {
          roleid = 2;
        } else if (this.data.role === 'Admin') {
          roleid = 3;
        }

        let userinfo;
        if (this.user.user_info.role_id === 1 || this.user.user_info.role_id === 2) {
          userinfo = {
            name: this.data.name,
            email: this.data.email,
            phone: this.data.phone,
            address: this.data.address,
            gender: this.data.gender,
            position: this.data.position,
            image: this.imageValue,
            role_id: roleid,
            verify: this.data.verify,
            verify_image: this.verifyimageValue
          };

        } else {
          userinfo = {
            user_id: this.data.id,
            name: this.data.name,
            email: this.data.email,
            phone: this.data.phone,
            address: this.data.address,
            gender: this.data.gender,
            position: this.data.position,
            image: this.imageValue,
            role_id: roleid,
            verify: this.data.verify,
            verify_image: this.verifyimageValue
          };
        }

        console.log(userinfo);
        this.alertUser.alertConfirmEditUser().then(resolve => {
          if (resolve) {
            this.userService.updateUser(userinfo).subscribe(data => {
              this.save = data;
              console.log(this.save);
              if (this.save.error) {
                this.alertUser.alertErrorEditUser();
              } else {
                this.alertUser.alertSuccessEditUser();
                this.onNoClick();
              }


            });
          }
        });

      } else if (this.data.verify === false && this.verifyimageValue !== '') {
        this.alertUser.alertSomeTextNull();
      } else if (this.data.verify !== false && this.verifyimageValue === '') {
        this.alertUser.alertSomeTextNull();
      }
    } else {
      this.alertUser.alertSomeTextNull();
    }
  }
}
