import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsershowRoutingModule } from './usershow-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UsershowRoutingModule
  ]
})
export class UsershowModule { }
