import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainpageRoutingModule } from './mainpage-routing.module';
import { MainpageComponent } from './mainpage.component';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../material-module';
import { LayoutModule } from '../../Layout/layout.module';
import { MainshowComponent } from './mainshow/mainshow.component';
import { RouterModule } from '@angular/router';
import { TutorialComponent } from './tutorial/tutorial.component';

@NgModule({
  declarations: [
    MainpageComponent,
    MainshowComponent,
    TutorialComponent
  ],
  imports: [
    CommonModule,
    MainpageRoutingModule,
    FormsModule,
    DemoMaterialModule,
    LayoutModule,
    RouterModule
  ]
})
export class MainpageModule { }
