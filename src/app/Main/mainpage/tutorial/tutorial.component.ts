import { Component, OnInit } from '@angular/core';
import { TutorialService } from '../../../Services/tutorial.service';
import { LoginModel } from '../../../Model/tutorial.model';
import { TutorialServices } from '../../../Shares/tutorial.service';
import { SaveModel } from '../../../Model/save.model';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})
export class TutorialComponent implements OnInit {

  tutorial: LoginModel
  newTitle = '';
  newDescription = '';
  save: SaveModel;
  keyword = '';
  constructor(
    private tutorialService: TutorialService,
    private alertTutorial: TutorialServices
  ) { }

  ngOnInit() {
    this.getTutorial();
  }

  getTutorial() {
    const params = {
      keyword: this.keyword
    };
    this.tutorialService.getTutorial(params).subscribe(data => {
      this.tutorial = data;
      console.log(this.tutorial);
    });
  }

  createTutorial() {
    let newTutorial = {
      title: this.newTitle,
      description: this.newDescription
    }
    this.tutorialService.createTutorial(newTutorial).subscribe(data => {
      this.save = data;
      console.log(this.save)
      this.alertTutorial.alertConfirmTutorial().then(resolve => {
        if (resolve) {
          this.alertTutorial.alertSuccessTutorial();
          this.getTutorial();
        }
      })
    })
  }

  deleteTutorial(id) {
    this.tutorialService.deleteTutorial(id).subscribe(data => {
      this.getTutorial();
    })
  }

}
