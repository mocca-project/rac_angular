import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyshowComponent } from './verifyshow.component';

describe('VerifyshowComponent', () => {
  let component: VerifyshowComponent;
  let fixture: ComponentFixture<VerifyshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
