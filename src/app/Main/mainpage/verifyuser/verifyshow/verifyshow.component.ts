import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../Services/users.service';
import { UserInfo } from '../../../../Model/user.model';
import { Host } from '../../../../../environments/environment';

@Component({
  selector: 'app-verifyshow',
  templateUrl: './verifyshow.component.html',
  styleUrls: ['./verifyshow.component.css']
})
export class VerifyshowComponent implements OnInit {

  HOST_URL = Host.url;
  user: UserInfo;
  isLoadUser = false;
  isHaveUserData = false;

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.getveriftuser();
  }

  getveriftuser() {
    this.isLoadUser = true;
    this.userService.getVerifyUser().subscribe(data => {
      this.user = data;

      const leng = Object.keys(this.user);
      if (leng.length > 0) {

        this.isHaveUserData = true;
        this.isLoadUser = false;
      } else {
        this.isHaveUserData = false;
        this.isLoadUser = false;
      }
      console.log(this.user);
    });
  }

}
