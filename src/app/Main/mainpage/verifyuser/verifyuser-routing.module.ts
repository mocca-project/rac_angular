import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerifyuserComponent } from './verifyuser.component';
import { VerifyshowComponent } from './verifyshow/verifyshow.component';
import { VerifydetailComponent } from './verifydetail/verifydetail.component';

const routes: Routes = [
  {
    path: '',
    component: VerifyuserComponent,
    children: [
      { path: '', component: VerifyshowComponent },
      { path: ':id', component: VerifydetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerifyuserRoutingModule { }
