import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../Services/users.service';
import { UserInfo } from '../../../../Model/user.model';
import { ActivatedRoute } from '@angular/router';
import { Host } from '../../../../../environments/environment';
import { UserService } from '../../../../Shares/user.service';
import { SaveModel } from '../../../../Model/save.model';

@Component({
  selector: 'app-verifydetail',
  templateUrl: './verifydetail.component.html',
  styleUrls: ['./verifydetail.component.css']
})
export class VerifydetailComponent implements OnInit {

  HOST_URL = Host.url;
  user: UserInfo;
  userId = this.route.snapshot.paramMap.get('id');
  roleDetail;
  save: SaveModel;

  showImageVerify;

  constructor(
    private userService: UsersService,
    private route: ActivatedRoute,
    private alertUser: UserService
  ) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getuserDetail(this.userId).subscribe(data => {
      this.user = data;

      if (this.user.role_id === 1) {
        this.roleDetail = 'User';
      } else if (this.user.role_id === 2) {
        this.roleDetail = 'Volunteer';
      } else if (this.user.role_id === 3) {
        this.roleDetail = 'Admin';
      }

      console.log(data);
    });
  }

  updateUser() {
    let roleid;
    if (this.roleDetail === 'User') {
      roleid = 1;
    } else if (this.roleDetail === 'Volunteer') {
      roleid = 2;
    }

    const userinfo = {
      user_id: this.user.id,
      name: this.user.name,
      email: this.user.email,
      phone: this.user.phone,
      address: this.user.address,
      gender: this.user.gender,
      position: this.user.position,
      role_id: roleid,
      verify: false
    };

    this.alertUser.alertConfirmEditUser().then(resolve => {
      if (resolve) {
        this.userService.updateUser(userinfo).subscribe(data => {
          this.save = data;
          console.log(this.save);
          if (this.save.error) {
            this.alertUser.alertErrorEditUser();
          } else {
            this.alertUser.alertSuccessEditUser();
            this.getUser();
          }


        });
      }
    });
  }

  selectImageVerify(image) {
    this.showImageVerify = image;
  }

}
