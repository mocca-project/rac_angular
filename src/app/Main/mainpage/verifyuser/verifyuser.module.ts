import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifyuserRoutingModule } from './verifyuser-routing.module';
import { VerifyuserComponent } from './verifyuser.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../../material-module';
import { VerifyshowComponent } from './verifyshow/verifyshow.component';
import { VerifydetailComponent } from './verifydetail/verifydetail.component';

@NgModule({
  declarations: [
    VerifyuserComponent,
    VerifyshowComponent,
    VerifydetailComponent
  ],
  imports: [
    CommonModule,
    VerifyuserRoutingModule,
    RouterModule,
    FormsModule,
    DemoMaterialModule
  ]
})
export class VerifyuserModule { }
