import { Component, OnInit, Inject } from '@angular/core';
import { PostService } from '../../../../Services/post.service';
import { PostModel } from '../../../../Model/post.model';
import { Host } from '../../../../../environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FavoritePostModel } from '../../../../Model/post.model';
import { FavoritepostService } from '../../../../Shares/favoritepost.service';
import { SaveModel } from '../../../../Model/save.model';

@Component({
  selector: 'app-postshow',
  templateUrl: './postshow.component.html',
  styleUrls: ['./postshow.component.css']
})
export class PostshowComponent implements OnInit {

  HOST_URL = Host.url;
  posts: PostModel;
  isLoadPost = false;
  isHavePostData = false;

  limit = 10;
  offset = 0;
  isBackForm = false;
  isNextForm = false;
  stackPage = 0;

  mylimit = 10;
  myoffset = 0;
  myisBackForm = false;
  myisNextForm = false;
  mystackPage = 0;

  constructor(
    private postService: PostService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getpost();
  }

  getpost() {
    this.isLoadPost = true;
    const param = {
      limit: this.limit,
      offset: this.offset
    }
    this.postService.getPost(param).subscribe(data => {
      console.log(data);
      this.posts = data;
      const leng = Object.keys(this.posts);
      if (leng.length > 0) {
        this.isHavePostData = true;
      } else {
        this.isHavePostData = false;
      }

      if (leng.length >= 10) {
        this.isNextForm = true;
      } else {
        this.isNextForm = false;
      }
      if (this.stackPage === 0) {
        this.isBackForm = false;
      } else {
        this.isBackForm = true;
      }

      this.isLoadPost = false;
    });
  }

  backPage() {
    this.stackPage -= 1;
    this.offset -= 10;
    this.posts = null;
    this.getpost();
  }

  nextPage() {
    this.isBackForm = true;
    this.stackPage += 1;
    this.offset += 10;
    this.posts = null;
    this.getpost();
  }

  getMyPost() {
    this.isLoadPost = true;
    const param = {
      limit: this.mylimit,
      offset: this.myoffset
    }
    this.postService.getMyPost(param).subscribe(data => {
      this.posts = data;
      const leng = Object.keys(this.posts);
      if (leng.length > 0) {
        this.isHavePostData = true;
      } else {
        this.isHavePostData = false;
      }

      if (leng.length >= 10) {
        this.myisNextForm = true;
      } else {
        this.myisNextForm = false;
      }
      if (this.mystackPage === 0) {
        this.myisBackForm = false;
      } else {
        this.myisBackForm = true;
      }

      this.isLoadPost = false;
    });
  }

  myBackPage() {
    this.mystackPage -= 1;
    this.myoffset -= 10;
    this.posts = null;
    this.getMyPost();
  }

  myNextPage() {
    this.myisBackForm = true;
    this.mystackPage += 1;
    this.myoffset += 10;
    this.posts = null;
    this.getMyPost();
  }

  checkTabSelect(event) {
    if (event.index === 0) { // posts
      this.getpost();
    } else if (event.index === 1) { // myposts
      this.getMyPost();
    }
  }

  openFavoritePost() {
    this.openDialog();
  }

  openDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(FavoritePostDialogComponent, {
      width: '500px',
      data: {

      }
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }
}

export interface DialogData {

}

@Component({
  selector: 'app-favoritepost-dialog',
  templateUrl: 'favoritepost-dialog.html',
})
export class FavoritePostDialogComponent implements OnInit {

  favoritepost: FavoritePostModel;
  isFavoritepost = false;
  save: SaveModel;

  constructor(
    public dialogRef: MatDialogRef<FavoritePostDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private postService: PostService,
    private alertFavoritePost: FavoritepostService
  ) { }

  ngOnInit() {
    this.getFavoritePost();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getFavoritePost() {
    this.postService.getFavoritePost().subscribe(data => {
      console.log(data);
      this.favoritepost = data;
      const leng = Object.keys(this.favoritepost);
      if (leng.length > 0) {
        this.isFavoritepost = true;
      } else {
        this.isFavoritepost = false;
      }
    });
  }

  deleteFavooritePost(favorId) {
    this.alertFavoritePost.alertConfirmDeleteFavoritePost().then(resolve => {
      if (resolve) {
        this.postService.deleteFavoritePost(favorId).subscribe(data => {
          this.save = data;
          if (this.save.error) {

          } else {
            this.alertFavoritePost.alertSuccessDeleteFavoritePost();
            this.getFavoritePost();
          }
        });
      }
    });
  }
}
