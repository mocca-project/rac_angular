import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostComponent } from '../post/post.component';
import { PostshowComponent } from './postshow/postshow.component';
import { PostcreateComponent } from './postcreate/postcreate.component';

const routes: Routes = [
  {
    path: '',
    component: PostComponent,
    children: [
      { path: '', component: PostshowComponent },
      { path: 'new', component: PostcreateComponent },
      {
        path: ':id',
        loadChildren: './postdetail/postdetail.module#PostdetailModule'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
