import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { PostComponent } from '../post/post.component';
import { DemoMaterialModule } from '../../../material-module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PostshowComponent, FavoritePostDialogComponent } from './postshow/postshow.component';
import { PostcreateComponent } from './postcreate/postcreate.component';

@NgModule({
  entryComponents: [FavoritePostDialogComponent],
  declarations: [
    PostComponent,
    PostshowComponent,
    PostcreateComponent,
    FavoritePostDialogComponent
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    DemoMaterialModule,
    FormsModule,
    RouterModule
  ]
})
export class PostModule { }
