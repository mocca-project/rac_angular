import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../../Services/post.service';
import { PostsService } from '../../../../Shares/post.service';
import { SaveModel } from '../../../../Model/save.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-postcreate',
  templateUrl: './postcreate.component.html',
  styleUrls: ['./postcreate.component.css']
})
export class PostcreateComponent implements OnInit {

  titlePost = '';
  descriptionPost = '';
  typePost = '';
  statusPost = '';
  naturePost = '';
  identifyPost = true;
  imagePost = [];

  titleLocation = '';
  latitudeLocation = '';
  longitudeLocation = '';

  // image
  imageValue: any = [];
  selectedFiles: File;

  // save
  save: SaveModel;

  constructor(
    private postService: PostService,
    private alertPost: PostsService,
    private routes: Router,
  ) { }

  ngOnInit() {
  }

  ImageToBase64(event) {
    const imgTable = [];
    if (event.target.files.length > 0) {
      this.imagePost = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < event.target.files.length; i++) {
        this.selectedFiles = event.target.files[i] as File;
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          // image show
          const img = evented.target.result;
          this.imagePost.push({
            image: img
          });

          // image value
          imgTable.push(evented.target.result);
          const imageText: any = [];
          if (imgTable.length > 0) {
            imgTable.forEach(image => {
              imageText.push(image.split(',')[1]);
            });
          }
          this.imageValue = imageText;
        };
        reader.readAsDataURL(this.selectedFiles);
      }
    }
  }


  createPost() {
    if (this.titlePost !== '' && this.descriptionPost !== '' && this.typePost !== '' && this.statusPost !== '' && this.naturePost !== '' && this.titleLocation !== '' && this.latitudeLocation !== '' && this.longitudeLocation !== '') {

      const postinfo = {
        post: {
          title: this.titlePost,
          description: this.descriptionPost,
          type_post: this.typePost,
          status: this.statusPost,
          nature: this.naturePost,
          identify: this.identifyPost,
          image: this.imageValue
        },
        location: {
          title: this.titleLocation,
          latitude: this.latitudeLocation,
          longitude: this.longitudeLocation
        }
      };
      console.log(postinfo);
      this.alertPost.alertConfirmCreatePost().then(resolve => {
        if (resolve) {
          this.postService.createPost(postinfo).subscribe(data => {
            this.save = data;
            if (this.save.error) {
              this.alertPost.alertErrorCreatePost();
            } else {
              this.alertPost.alertSuccessCreatePost();
              this.routes.navigate(['../main/posts']);
            }
          });
        }
      });

    } else {
      this.alertPost.alertSomePostNull();
    }
  }

}
