import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostdetailRoutingModule } from './postdetail-routing.module';
import { PostdetailComponent, EditPostDialogComponent } from './postdetail.component';
import { DemoMaterialModule } from '../../../../material-module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  entryComponents: [EditPostDialogComponent],
  declarations: [
    PostdetailComponent,
    EditPostDialogComponent
  ],
  imports: [
    CommonModule,
    PostdetailRoutingModule,
    DemoMaterialModule,
    FormsModule,
    RouterModule
  ]
})
export class PostdetailModule { }
