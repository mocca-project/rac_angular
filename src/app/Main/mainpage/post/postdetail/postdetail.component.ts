import { Component, OnInit, Inject } from '@angular/core';
import { PostService } from '../../../../Services/post.service';
import { ActivatedRoute } from '@angular/router';
import { PostDetail } from '../../../../Model/postdetail.model';
import { Host } from '../../../../../environments/environment';
import { PostsService } from '../../../../Shares/post.service';
import { SaveModel } from '../../../../Model/save.model';
import { Router } from '@angular/router';
import { CommentService } from '../../../../Services/comment.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommentsService } from '../../../../Shares/comments.service';
import { TohelpService } from '../../../../Services/tohelp.service';
import { TohelpsService } from '../../../../Shares/tohelps.service';
import { ToHelpModel } from '../../../../Model/tohelp.model';
import { FavoritepostService } from '../../../../Shares/favoritepost.service';

@Component({
  selector: 'app-postdetail',
  templateUrl: './postdetail.component.html',
  styleUrls: ['./postdetail.component.css']
})
export class PostdetailComponent implements OnInit {

  HOST_URL = Host.url;
  post: PostDetail;
  postId = this.route.snapshot.paramMap.get('id');

  isLoadPostDetail = false;
  // save
  save: SaveModel;
  commentValue = '';
  isSubmitClicked = false;

  // image
  imageValue;
  imageShow;
  selectedFiles: File;

  // edit
  idEdit;
  titleEdit;
  descriptionEdit;
  typepostEdit;
  statusEdit;
  natureEdit;
  imageEdit = [];
  imageShowEdit;
  titleLocationEdit;
  latitudeEdit;
  longitudeEdit;

  tohelp: ToHelpModel;
  tohelpHaveData = false;

  // show image posts
  showImage;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private alertPost: PostsService,
    private routes: Router,
    private commentService: CommentService,
    public dialog: MatDialog,
    private alertComment: CommentsService,
    private tohelpService: TohelpService,
    private alertToHelp: TohelpsService,
    private alertFavoritePost: FavoritepostService
  ) { }

  ngOnInit() {
    this.getpostdetail();
    this.gettohelp();
  }

  getpostdetail() {
    this.isLoadPostDetail = true;
    this.postService.getPostDetail(this.postId).subscribe(data => {
      this.post = data;
      console.log(data);
      this.idEdit = this.post.id;
      this.titleEdit = this.post.title;
      this.descriptionEdit = this.post.description;
      this.typepostEdit = this.post.type_post;
      this.statusEdit = this.post.status;
      this.natureEdit = this.post.nature;
      if (this.post.images) {
        this.post.images.forEach(img => {
          this.imageEdit.push({
            id: img.id,
            image: this.HOST_URL + img.image
          });
        });
      }
      this.imageShowEdit = this.HOST_URL + this.post.images[0].image;
      this.titleLocationEdit = this.post.location.title;
      this.latitudeEdit = this.post.location.latitude;
      this.longitudeEdit = this.post.location.longitude;

      this.isLoadPostDetail = false;
    });
  }

  gettohelp() {
    this.tohelpService.getToHelp(this.postId).subscribe(data => {
      console.log(data);
      this.tohelp = data;
      const leng = Object.keys(this.tohelp);
      if (leng.length) {
        this.tohelpHaveData = true;
      } else {
        this.tohelpHaveData = false;
      }
    });
  }

  createtohelp() {
    this.alertToHelp.alertConfirmToHelp().then(resolve => {
      if (resolve) {
        this.tohelpService.createToHelp(this.postId).subscribe(data => {
          this.save = data;
          if (this.save.error) {
          } else {
            this.alertToHelp.alertSuccessToHelp(this.save.message);
            this.gettohelp();
          }
        });
      }
    });
  }

  deletetohelp(helpId) {
    this.alertToHelp.alertConfirmDeleteToHelp().then(resolve => {
      if (resolve) {
        this.tohelpService.deleteToHelp(helpId, this.postId).subscribe(data => {
          this.save = data;
          if (this.save.error) {

          } else {
            this.alertToHelp.alertSuccessDeleteToHelp();
            this.gettohelp();
          }
        });
      }
    });
  }

  deletePost() {
    this.alertPost.alertConfirmDeletePost().then(resolve => {
      if (resolve) {
        this.postService.deletePost(this.postId).subscribe(data => {
          this.save = data;
          console.log(data);
          if (this.save.error) {
            this.alertPost.alertErrorDeletePost();
          } else {
            this.alertPost.alertSuccessDeletePost();
            this.routes.navigate(['../main/posts']);
          }
        });
      }
    });
  }

  ImageToBase64(event) {
    this.selectedFiles = event.target.files[0] as File;
    if (this.selectedFiles.size < 10 * 1024 * 1024) {
      const reader = new FileReader();
      reader.onload = (evented: any) => {
        this.imageShow = evented.target.result;
        this.imageValue = this.imageShow.split(',')[1];
      };
      reader.readAsDataURL(this.selectedFiles);
    } else {
      // this.imageService.alertImageOverSize();
    }
  }


  comment() {

    this.isSubmitClicked = true;
    const commentinfo = {
      post_id: this.postId,
      description: this.commentValue,
      image: this.imageValue
    };
    // console.log(commentinfo);

    this.commentService.createComment(commentinfo).subscribe(data => {
      this.save = data;
      this.isSubmitClicked = false;
      if (this.save.error) {
        this.getpostdetail();
      } else {
        // console.log(this.save);
        this.imageShow = null;
        this.getpostdetail();
      }
    });

  }

  deleteComment(id) {
    this.alertComment.alertConfirmDeleteComment().then(resolve => {
      if (resolve) {
        this.commentService.deleteComment(this.postId, id).subscribe(data => {
          console.log(data);
          this.getpostdetail();
        });
      }
    });
  }

  editPost() {
    this.openDialog();
  }

  openDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(EditPostDialogComponent, {
      width: '500px',
      data: {
        id: this.idEdit,
        title: this.titleEdit,
        description: this.descriptionEdit,
        type_post: this.typepostEdit,
        status: this.statusEdit,
        nature: this.natureEdit,
        image: this.imageEdit,
        imageshow: this.imageShowEdit,
        titleLocation: this.titleLocationEdit,
        latitude: this.latitudeEdit,
        longitude: this.longitudeEdit
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getpostdetail();
    });
  }

  addFavoritePost() {
    this.alertFavoritePost.alertConfirmFavoritePost().then(resolve => {
      if (resolve) {
        this.postService.createFavoritePost(this.postId).subscribe(data => {
          this.save = data;
          if (this.save.error) {

          } else {
            this.alertFavoritePost.alertSuccessFavoritePost();
          }
        })
      }
    });
  }

  selectImageProfile(image) {
    this.showImage = image;
  }
}

export interface DialogData {
  id: any;
  title: any;
  description: any;
  type_post: any;
  status: any;
  nature: any;
  image: any;
  imageshow: any;
  titleLocation: any;
  latitude: any;
  longitude: any;
}

@Component({
  selector: 'app-detailedit-dialog',
  templateUrl: 'postdetailedit-dialog.html',
})
export class EditPostDialogComponent implements OnInit {

  // save
  save: SaveModel;
  // image
  imageValue = [];
  selectedFiles: File;

  constructor(
    public dialogRef: MatDialogRef<EditPostDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private postService: PostService,
    private alertPost: PostsService
  ) { }

  ngOnInit() {
    console.log(this.data.image);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ImageToBase64(event) {
    const imgTable = [];
    if (event.target.files.length > 0) {
      this.data.image = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < event.target.files.length; i++) {
        this.selectedFiles = event.target.files[i] as File;
        const reader = new FileReader();
        reader.onload = (evented: any) => {
          const img = evented.target.result;
          this.data.image.push({
            image: img
          }); // show new image

          // this.data.image = evented.target.result;
          // this.imageValue = this.data.image.split(',')[1];
          imgTable.push(evented.target.result);
          const imageText: any = [];
          if (imgTable.length > 0) {
            imgTable.forEach(element => {
              imageText.push(element.split(',')[1]);
            });
          }
          this.imageValue = imageText;
          console.log(this.imageValue);
        };
        reader.readAsDataURL(this.selectedFiles);
      }
    }

  }

  updatePost() {
    let postinfo;
    if (this.imageValue.length > 0) { // change image
      postinfo = {
        post: {
          title: this.data.title,
          description: this.data.description,
          type_post: this.data.type_post,
          status: this.data.status,
          nature: this.data.nature,
          image: this.imageValue
        },
        location: {
          title: this.data.titleLocation,
          latitude: this.data.latitude,
          longitude: this.data.longitude
        }
      };
    } else { // not change image
      postinfo = {
        post: {
          title: this.data.title,
          description: this.data.description,
          type_post: this.data.type_post,
          status: this.data.status,
          nature: this.data.nature
        },
        location: {
          title: this.data.titleLocation,
          latitude: this.data.latitude,
          longitude: this.data.longitude
        }
      };
    }

    // console.log(postinfo);
    this.alertPost.alertConfirmUpdatePost().then(resolve => {
      if (resolve) {
        this.postService.updatePost(postinfo, this.data.id).subscribe(data => {
          this.save = data;
          if (this.save.error) {

          } else {
            console.log(this.save);
            this.alertPost.alertSuccessUpdatePost();
            this.onNoClick();
          }
        });
      }

    });

  }
}
