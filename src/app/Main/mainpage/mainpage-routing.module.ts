import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage.component';
import { MainshowComponent } from './mainshow/mainshow.component';
import { TutorialComponent } from './tutorial/tutorial.component';

const routes: Routes = [
  {
    path: '',
    component: MainpageComponent,
    children: [
      { path: '', component: MainshowComponent },
      { path: 'tutorial', component: TutorialComponent },
      {
        path: 'users',
        loadChildren: './user/user.module#UserModule'
      },
      {
        path: 'posts',
        loadChildren: './post/post.module#PostModule'
      },
      {
        path: 'verifyusers',
        loadChildren: './verifyuser/verifyuser.module#VerifyuserModule'
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainpageRoutingModule { }
