import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainshowComponent } from './mainshow.component';

describe('MainshowComponent', () => {
  let component: MainshowComponent;
  let fixture: ComponentFixture<MainshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
