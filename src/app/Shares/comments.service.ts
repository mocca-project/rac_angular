import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor() { }

  alertConfirmDeleteComment() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure delete comment?',
        text: 'Confirm delete this comment.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }


}

