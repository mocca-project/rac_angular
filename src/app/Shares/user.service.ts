import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  alertConfirmEditUser() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure edit user?',
        text: 'Confirm edit this user.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertErrorEditUser() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'Can not edit this user',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertSuccessEditUser() {
    Swal.fire({
      title: 'Edit Successed!',
      text: 'Your edit this user successed.',
      type: 'success',
    });
  }

  alertConfirmCreateUser() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure create user?',
        text: 'Confirm create this user.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessCreateUser() {
    Swal.fire({
      title: 'Create Successed!',
      text: 'Your create this user successed.',
      type: 'success',
    });
  }

  alertErrorCreateUser() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'Can not create this user',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertNotSamePassword() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'Password is not match',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertConfirmDeleteUser() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure delete user?',
        text: 'Confirm delete this user.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessDeleteUser() {
    Swal.fire({
      title: 'Delete Successed!',
      text: 'Your delete this user successed.',
      type: 'success',
    });
  }

  alertSomeTextNull() {
    Swal.fire({
      title: 'Some input is null!',
      text: 'Can not create this user',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }


}
