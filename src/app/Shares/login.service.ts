import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthService } from '../Services/auth.service';

// ngrx
import { Store } from '@ngrx/store';
import { AppState } from '../ngrxstore/user.state';
import * as UserActions from '../ngrxstore/user.actions';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private authService: AuthService,
    private store: Store<AppState>,
    private routes: Router,
  ) { }

  alertLoginFail() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'username or password is wrong.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }
  alertLoginIsNull() {
    Swal.fire({
      title: 'Username or password is empty!',
      text: 'please fill username or password and try again.',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertLogoutConfirm() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Logout?',
        text: 'Confirm logout.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          resolve(result.value);
          this.authService.authsignout().subscribe(data => {
            this.store.dispatch(new UserActions.RemoveUser());
          });
          this.routes.navigate(['/']);
        }
      });
    });
    return promise;
  }

  alertAdminOnly() {
    Swal.fire({
      title: 'Admin only!',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }
}
