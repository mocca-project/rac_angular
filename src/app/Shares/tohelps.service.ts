import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class TohelpsService {

  constructor() { }

  alertConfirmToHelp() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure to help?',
        text: 'Confirm to help.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessToHelp(message) {
    Swal.fire({
      title: message,
      type: 'success',
    });
  }

  alertConfirmDeleteToHelp() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure cancel to help?',
        text: 'Confirm cancel to help.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessDeleteToHelp() {
    Swal.fire({
      title: 'Cancel To Help Successed!',
      text: 'Your Cancel to help successed.',
      type: 'success',
    });
  }

}
