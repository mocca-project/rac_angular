import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class FavoritepostService {

  constructor() { }

  alertConfirmFavoritePost() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure add favorite post?',
        text: 'Confirm add favorite post.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessFavoritePost() {
    Swal.fire({
      title: 'Add Favorite Post Successed!',
      text: 'Your add favorite post successed.',
      type: 'success',
    });
  }

  alertConfirmDeleteFavoritePost() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure delete favorite post?',
        text: 'Confirm delete favorite post.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessDeleteFavoritePost() {
    Swal.fire({
      title: 'Delete Favorite Post Successed!',
      text: 'Your delete favorite post successed.',
      type: 'success',
    });
  }

}


