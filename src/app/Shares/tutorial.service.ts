import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class TutorialServices {

  constructor() { }

  alertConfirmTutorial() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure create tutorial?',
        text: 'Confirm create tutorial.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessTutorial() {
    Swal.fire({
      title: 'create success',
      type: 'success',
    });
  }

}


