import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor() { }

  alertConfirmCreatePost() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure create post?',
        text: 'Confirm create this post.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertErrorCreatePost() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'Can not create this post',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertSuccessCreatePost() {
    Swal.fire({
      title: 'Create Successed!',
      text: 'Your create this post successed.',
      type: 'success',
    });
  }

  alertConfirmDeletePost() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure delete post?',
        text: 'Confirm delete this post.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertErrorDeletePost() {
    Swal.fire({
      title: 'Something wrong!',
      text: 'Can not delete this post',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

  alertSuccessDeletePost() {
    Swal.fire({
      title: 'Delete Successed!',
      text: 'Your delete this post successed.',
      type: 'success',
    });
  }

  alertConfirmUpdatePost() {
    const promise = new Promise((resolve, reject) => {
      Swal.fire({
        title: 'Are you sure update post?',
        text: 'Confirm update this post.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3885DE',
        cancelButtonColor: '#e41c44',
        confirmButtonText: 'Yes',
      }).then((result) => {
        resolve(result.value);
      });
    });
    return promise;
  }

  alertSuccessUpdatePost() {
    Swal.fire({
      title: 'Update Successed!',
      text: 'Your update this post successed.',
      type: 'success',
    });
  }

  alertSomePostNull() {
    Swal.fire({
      title: 'Some text is null',
      text: 'Can not create this post',
      type: 'error',
      confirmButtonColor: '#3885DE',
      confirmButtonText: 'OK'
    });
  }

}
