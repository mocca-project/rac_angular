import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

import { Store } from '@ngrx/store';
import { AppState } from '../ngrxstore/user.state';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  getUser() {
    return this.store.select('user');
  }

  getuserService(param) {
    return this.http.get(HOST_URL + '/users', { params: param });
  }

  getVerifyUser() {
    return this.http.get(HOST_URL + '/verifyusers');
  }

  getuserDetail(id) {
    return this.http.get(HOST_URL + '/users/' + id);
  }

  updateUser(userinfo) {
    return this.http.put(HOST_URL + '/users', { user: userinfo });
  }

  createUser(userinfo) {
    return this.http.post(HOST_URL + '/users', { user: userinfo });
  }

  deleteUser(id) {
    return this.http.delete(HOST_URL + '/users/' + id);
  }
}
