import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(
    private http: HttpClient,
  ) { }

  getTutorial(param) {
    return this.http.get(HOST_URL + '/tutorials', { params: param });
  }

  createTutorial(data) {
    return this.http.post(HOST_URL + '/tutorials', data);
  }

  deleteTutorial(id) {
    return this.http.delete(HOST_URL + '/tutorials/' + id);
  }

}
