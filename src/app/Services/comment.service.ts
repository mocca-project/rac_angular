import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  createComment(commentinfo) {
    return this.http.post(HOST_URL + '/comments', { comment: commentinfo });
  }

  deleteComment(postId, id) {
    return this.http.delete(HOST_URL + '/posts/ ' + postId + '/comments/' + id);
  }
}
