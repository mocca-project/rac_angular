import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { User } from '../Model/user.model';
import { Router } from '@angular/router';
// ngrx
import { Store } from '@ngrx/store';
import { AppState } from '../ngrxstore/user.state';
import * as UserActions from '../ngrxstore/user.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  user: User;
  localUser: any;

  constructor(
    private routes: Router,
    private store: Store<AppState>
  ) {
    this.store.select('user').subscribe(data => {
      this.user = data;
    });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (state.url === '/') {
      if (this.user !== null) { // signin
        this.routes.navigate(['/main']);
      }
    } else if (state.url === '/signin') {
      if (this.user !== null) { // signin
        this.routes.navigate(['/main']);
      }

    } else {
      if (this.user === null) { // signout
        this.routes.navigate(['/signin']);
      }
    }
    return true;
  }
}
