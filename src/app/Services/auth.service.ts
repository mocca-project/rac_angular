import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  authsignin(data) {
    const params = { session: data };
    return this.http.post(HOST_URL + '/login', params);
  }

  authsignout() {
    return this.http.delete(HOST_URL + '/logout');
  }

}
