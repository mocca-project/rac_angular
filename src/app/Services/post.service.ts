import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPost(param) {
    return this.http.get(HOST_URL + '/posts', { params: param });
  }

  getMyPost(param) {
    return this.http.get(HOST_URL + '/myposts', { params: param });
  }

  getPostDetail(postid) {
    return this.http.get(HOST_URL + '/posts/' + postid);
  }

  createPost(postinfo) {
    return this.http.post(HOST_URL + '/posts', postinfo);
  }

  deletePost(id) {
    return this.http.delete(HOST_URL + '/posts/' + id);
  }

  updatePost(postinfo, id) {
    return this.http.put(HOST_URL + '/posts/' + id, postinfo);

  }

  getFavoritePost() {
    return this.http.get(HOST_URL + '/favoriteposts');
  }

  createFavoritePost(postId) {
    return this.http.post(HOST_URL + '/favoriteposts', { post_id: postId });
  }

  deleteFavoritePost(favorId) {
    return this.http.delete(HOST_URL + '/favoriteposts/' + favorId);
  }
}
