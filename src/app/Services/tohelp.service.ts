import { Injectable } from '@angular/core';
import { Host } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

const HOST_URL = Host.url;

@Injectable({
  providedIn: 'root'
})
export class TohelpService {

  constructor(private http: HttpClient) { }

  getToHelp(postId) {
    return this.http.get(HOST_URL + '/posts/' + postId + '/tohelps');
  }

  createToHelp(postId) {
    return this.http.post(HOST_URL + '/posts/' + postId + '/tohelps', { post_id: postId });
  }

  deleteToHelp(helpId, postId) {
    return this.http.delete(HOST_URL + '/posts/' + postId + '/tohelps/' + helpId)
  }
}
