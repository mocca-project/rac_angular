import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Shares/login.service';
import { User } from '../../Model/user.model';
import { Host } from '../../../environments/environment';
// ngrx
import { Store } from '@ngrx/store';
import { AppState } from '../../ngrxstore/user.state';
import * as UserActions from '../../ngrxstore/user.actions';

import { UsersService } from '../../Services/users.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  HOST_URL = Host.url;
  user: User;

  constructor(
    private loginService: LoginService,
    private store: Store<AppState>,
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.getuser();
  }

  getuser() {
    this.userService.getUser().subscribe(state => {
      this.user = state;
      if (this.user) {
        this.notifications(this.user);
      }
      console.log(this.user);
    });
  }

  notifications(user) {
    // tslint:disable-next-line:no-string-literal
    const OneSignal = window['OneSignal'] || [];
    // tslint:disable-next-line:only-arrow-functions
    OneSignal.push(function () {
      OneSignal.init({
        appId: '9022bdcb-16a4-4f23-a3ed-04d3fccb16dc'
      });
      // tslint:disable-next-line:only-arrow-functions
      OneSignal.sendTag('role_id', user.user_info.role_id).then(function (tagsSent) {
        console.log(tagsSent);
      });
    });
  }

  onButtonClick($event) {
    const clickedElement = $event.target || $event.srcElement;

    if (clickedElement.nodeName === 'BUTTON') {
      const isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector('.active');
      // if a Button already has Class: .active
      if (isCertainButtonAlreadyActive) {
        isCertainButtonAlreadyActive.classList.remove('active');
      }

      clickedElement.className += ' active';
    }
  }

  logout() {
    this.loginService.alertLogoutConfirm().then(data => {
    });
  }


}
