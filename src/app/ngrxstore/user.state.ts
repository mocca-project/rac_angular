import { User } from '../Model/user.model';

export interface AppState {
  readonly user: User;
}

