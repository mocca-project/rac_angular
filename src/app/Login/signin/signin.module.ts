import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigninRoutingModule } from './signin-routing.module';
import { SigninComponent } from '../signin/signin.component';
import { DemoMaterialModule } from '../../material-module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SigninComponent
  ],
  imports: [
    CommonModule,
    SigninRoutingModule,
    DemoMaterialModule,
    FormsModule
  ]
})
export class SigninModule { }
