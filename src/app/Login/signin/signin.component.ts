import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../Model/login.model';
import { LoginService } from '../../Shares/login.service';
import { AuthService } from '../../Services/auth.service';
import { Router } from '@angular/router';

// ngrx
import { Store } from '@ngrx/store';
import { AppState } from '../../ngrxstore/user.state';
import * as UserActions from '../../ngrxstore/user.actions';

declare var FB: any;

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  login: LoginModel = {
    username: '',
    password: ''
  };
  user: any;
  isPassHide = true;
  isLoading = false;

  constructor(
    private routes: Router,
    private loginService: LoginService,
    private authService: AuthService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {

    this.store.dispatch(new UserActions.RemoveUser());
    // this.store.dispatch(new UserActions.RemoveUser());

    // (window as any).fbAsyncInit = function () {
    //   FB.init({
    //     appId: '337437293520062',
    //     cookie: true,
    //     xfbml: true,
    //     version: 'v3.1'
    //   });
    //   FB.AppEvents.logPageView();
    // };


    // console.log(JSON.parse(localStorage.getItem('FB')));
  }

  // submitLogin() {
  //   this.FBLogin().then(resolve => {
  //     let login = {
  //       token_fb: resolve
  //     };
  //     this.authService.authsignin(login).subscribe(data => {
  //       console.log(data);
  //     });
  //   })
  // }

  // FBLogin() {
  //   console.log("submit login to facebook");
  //   const promise = new Promise((resolve, reject) => {
  //     FB.login((response) => {
  //       console.log('submitLogin', response);
  //       if (response.authResponse) {
  //         console.log('login success!');
  //         console.log(response.authResponse);
  //         // localStorage.setItem('FB', JSON.stringify(response));
  //         resolve(response.authResponse.accessToken);

  //       } else {
  //         console.log('User login failed');
  //       }
  //     });
  //   });
  //   return promise;
  // }

  // checkUser() {
  //   FB.api('/me', function (response) {
  //     console.log(response);
  //   });
  // }

  // checkLoginState() {
  //   FB.getLoginStatus(function (response) {
  //     if (response.status === 'connected') {
  //       console.log(response);
  //     } else {
  //       console.log(response);
  //     }
  //   });
  // }

  // LogoutFB() {
  //   FB.logout(function (response) {
  //     // Person is now logged out
  //     console.log(response);
  //   });
  // }

  signin() {
    this.isLoading = true;
    if (this.login.username && this.login.password) {
      this.authService.authsignin(this.login).subscribe(data => {
        this.user = data;
        if (this.user) {
          if (!this.user.error) {
            if (this.user.user_info.role_id === 3) {
              this.store.dispatch(new UserActions.AddUser(this.user));
              this.store.select('user').subscribe(data => {

                this.user = data;
              });
              this.routes.navigate(['/main']);
            } else {
              this.loginService.alertAdminOnly();
            }

          } else {
            this.login.username = '';
            this.login.password = '';
          }
          this.isLoading = false;
        }

      }, (error) => {
        this.isLoading = false;
        this.loginService.alertLoginFail();
      });
    } else {
      this.isLoading = false;
      this.loginService.alertLoginIsNull();
    }
  }

}
