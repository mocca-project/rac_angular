export interface User {
  user_info?: UserInfo;
  token?: string;
  error?: string;
}

export interface UserInfo {
  id?: number;
  username?: string;
  address?: string;
  email?: string;
  gender?: string;
  image?: string;
  name?: string;
  phone?: string;
  position?: string;
  role_id?: number;
  verify?: boolean;
  verify_image?: string;
}
