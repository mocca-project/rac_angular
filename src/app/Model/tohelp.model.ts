export interface ToHelpModel {
  id?: number;
  post_id?: number;
  user_id?: number;
  name?: string;
  created_at?: Date;
  updated_at?: Date;
}
