export interface SaveModel {
  message?: string;
  error?: string;
}
