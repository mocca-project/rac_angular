export interface PostDetail {
  id?: number;
  title?: string;
  description?: string;
  type_post?: string;
  status?: string;
  nature?: string;
  identify?: boolean;
  images?: Image[];
  creater?: Creater;
  created_at?: Date;
  updated_at?: Date;
  location?: Location;
  comments?: Comment[];
}

export interface Image {
  id?: number;
  image?: string;
}

export interface Creater {
  user_id?: number;
  name?: string;
  role_id?: number;
  image?: string;
}

export interface Location {
  id?: number;
  title?: string;
  latitude?: string;
  longitude?: string;
}

export interface Comment {
  id?: number;
  post_id?: number;
  user_id?: number;
  description?: string;
  name?: string;
  role_id?: number;
  image_user?: string;
  image?: string;
  created_at?: Date;
  updated_at?: Date;
}


