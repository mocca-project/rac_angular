export interface PostModel {
  id?: number;
  title?: string;
  description?: string;
  type_post?: string;
  status?: string;
  nature?: string;
  images?: Image[];
  creater?: Creater;
  created_at?: Date;
  updated_at?: Date;
  location?: Location;
}
export interface Location {
  id?: number;
  post_id?: number;
  title?: string;
  latitude?: string;
  longitude?: string;
}

export interface Creater {
  user_id?: number;
  name?: string;
  role_id?: number;
}

export interface Image {
  id?: number;
  image?: string;
}

export interface FavoritePostModel {
  id?: number;
  user_id?: number;
  post?: Post;
  created_at?: string;
  updated_at?: string;
}
interface Post {
  post_id?: number;
  title?: string;
  description?: string;
  type_post?: string;
  status?: string;
  nature?: string;
  image?: null;
  location?: Location;
}




